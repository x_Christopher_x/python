import numpy




# # a=numpy.array([1,2,3,4])
# # # b=[1,2,3,4]
# # b=numpy.append(a,[5,6,7,8])

# # print(a)
# # print(b)

# m=numpy.array([[1,-2,3],[4,-5,6]])
# # m2=numpy.array([[7],[7]])
# # m3=numpy.append(m,m2,axis=1)
# # print(m)
# # print(m2)
# # print(m3)

# #ELIMINAR COLUMNA
# a=numpy.delete(m,1,axis=1)
# print("#ELIMINAR COLUMNA")
# print(m)
# print("___________ Resultado __")
# print(a)

# #ELIMINAR FILA
# a=numpy.delete(m,1,axis=0)
# print("#ELIMINAR FILA")
# print(m)
# print("___________ Resultado __")
# print(a)

# #COMPROBAR SI LA MATRIZ ESTA VACIA

# if (m.size==0):
#     print("Matriz vacia")
# else:
#     print("Matriz con elementos")
# #OBTENER INDICES
# indice=numpy.where(m==6)
# print("el indice de el valor 1 es =", indice)

# #LONGITUDE UN ARREGLO
# longitud=m.size
# print("la longitud de la matriz m es ",longitud)

# #CREAR UN ARREGLO NUMPY DESDE UNA LISTA
# lista=[1,200,-4,400,5,6,2,1000]
# nnum=numpy.array(lista)
# print("esta es un array numpy ",nnum)
# #CONVERTIR UNA MATRIZ NUMPY A LISTA
# listanormal=nnum.tolist()
# print("lista normal",listanormal)
# #ORDENAR ARREGLOS EN NUMPY
# print("la matriz ordenada es ",numpy.sort(nnum))
# print("la matriz ordenada es ",numpy.sort(m))

# # aa=numpy.array([2,4,5])
# # bb=numpy.array([3,3,3])
# # print(aa*bb)


# aa=numpy.array([[2,3,4],[4,4,4],[5,3,1]])
# bb=numpy.array([[3,1,1],[1,2,2],[1,1,0]])

# suma=aa+bb
# multi=aa*bb
# print(suma)
# print(multi)