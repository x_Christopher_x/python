#   Proyecto creado por TecnoProfe
#    youtube: https://www.youtube.com/tecnoprofe
import pygame
import os

ANCHO = 1024
ALTO = 700
DIMENSIONES=(ANCHO,ALTO)
NEGRO = (0, 0, 0)
pygame.init()
ventana = pygame.display.set_mode(DIMENSIONES)
pygame.display.set_caption("Aviones")
clock = pygame.time.Clock()

# ========================================
# CLASE NAVE
class Nave(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave2.png")
        self.image.set_colorkey(NEGRO)
        self.rect = self.image.get_rect()
        self.rect.centerx = ANCHO // 2
        self.rect.bottom = ALTO-2
        

    def update(self):
        self.speed_x = 0
        keystate=pygame.key.get_pressed()        

        self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave1.png")

        if keystate[pygame.K_LEFT]:
            self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave1_izq.png")
            self.speed_x = -3
        if keystate[pygame.K_RIGHT]:
            self.image = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\nave1_der.png")
            self.speed_x = 3           
        self.rect.x += self.speed_x        
        
        # Control de salida de los bordes izquierdo y derecho.
        if self.rect.right > ANCHO:
            self.rect.right = ANCHO
        if self.rect.left < 0:
            self.rect.left = 0
           
# ========================================
# FUNCIÓN PARA SALIR DEL JUEGO
def salir():    
    fuente = pygame.font.Font('freesansbold.ttf', 32)    
    texto_salir = fuente.render("Si desea salir... presione la tecla 's'", True, (195,157,154),(0,0,45))        
    while True:                   
        for evento in pygame.event.get():                         
            if evento.type == pygame.KEYDOWN:                 
                if evento.key == pygame.K_s:                     
                    quit()     
                else:
                    return 0                
        ventana.fill((158,185,65))        
        ventana.blit(texto_salir,(ANCHO//4,ALTO//2))
        pygame.display.flip()

# ========================================
# FOndo de pantalla
fondo = pygame.image.load(os.path.dirname(__file__)+"\\img\\nave\\fondo03.png")

# ========================================
# Creación de la nave
all_sprites = pygame.sprite.Group()
avion = Nave()
all_sprites.add(avion)

# ========================================
# BUCLE PRINCIPAL DEL JUEGO
while True:   
    # velocidad sujerida del juego
    clock.tick(120)
    # obtiene eventos de entrada
    for evento in pygame.event.get():                         
        if evento.type == pygame.KEYDOWN: 
            # Control de salida con la tecla escape
            if evento.key == pygame.K_ESCAPE: 
                salir()
                #quit()      
    # Actualiza los sprite
    all_sprites.update()
    #Renderiza fondo noegro
    ventana.blit(fondo,(0,0))
    all_sprites.draw(ventana)    
    # dibujando todo
    pygame.display.flip()