import ply.lex as lex


#TOKENS
tokens = [  'IGNORE',
            'PLUS', 
            'MINUS', 
            'ASTERISK', 
            'SLASH', 
            'LPAREN', 
            'RPAREN', 
            'LKEY',
            'RKEY',
            'LCORCHETE',
            'RCORCHETE',
            'QUOTE',
            'QUOTESINGLE',
            'MAJOR',
            'MINOR',
            'DIFERENT',
            'COMPARERORDER',
            'UNOUOTRO',
            'ASSIGNMENT',
            'EQUAL',
            'IDENTICAL',
            'DENIAL',
            'DISTINT',
            'UNDERSCORE',
            'COMA',
            'PUNTOCOMA',
            'DOT',
            'DOUBLEDOT',
            'Y',
            'O',
            'MOD', 
            'DOLLAR',
            'NUMBER',
            'BIN',
            'CHARACTER',
            'CHAIN',
            'VARIABLE',
            'COMMENTARY',
            'START',
            'END',
            #PALABRAS RESERVADAS
            'ARRAY',
            'FUNCTION',
            'OBJECT',
            'NULL',
            'INT',
            'INTEGER',
            'DOUBLE',
            'FLOAT',
            'CHAR',
            'STRING',
            'BOOLEAN',
            'IF',
            'ELSE',
            'FOR',
            'FOREACH',
            'WHILE',
            'DO',
            'SWITCH',
            'CASE',
            'BREAK',
            'DEFAULT',
            'RETURN',
            'ECHO',
            'TYPE',
            'AS',
            'CLASS',
            'AND',
            'OR',
            'XOR',
            'SET',
            'GET',
            'SETTYPE',
            'COOKIES',
            'SETCOOKIES',
            'POST',
            'PUSH',
            'REQUIRE',
            'INCLUDE',
            'DATETIME',
            'DATE',
            'TIME'
            ]


#REGLAS SIMPLES
t_IGNORE = r"[ ]+"
t_PLUS = r"\+"
t_MINUS = r"\-"
t_ASTERISK = r"\*"
t_SLASH = r"\/"
t_LPAREN = r"\("
t_RPAREN = r"\)"
t_LKEY = r"\{"
t_RKEY = r"\}"
t_LCORCHETE = r"\[" 
t_RCORCHETE = r"\]"
t_QUOTE = r"\""
t_QUOTESINGLE = r"\'"
t_MAJOR = r"\>"
t_MINOR = r"\<"
t_DIFERENT = r"\<\>"
t_COMPARERORDER = r"\<\=\>"
t_UNOUOTRO = r"\?\?"
t_ASSIGNMENT = r"\="
t_EQUAL = r"\={2}"
t_IDENTICAL = r"\={3}"
t_DENIAL = r"\!"
t_DISTINT = r"\!\="
t_UNDERSCORE = r"\_"
t_COMA = r"\,"
t_PUNTOCOMA = r"\;"
t_DOT = r"\."
t_DOUBLEDOT = r"\:"
t_Y = r"\&\&"
t_O = r"\|\|"
t_MOD = "[%]"
t_DOLLAR = "[$]"
t_ARRAY = "(array)"
t_FUNCTION = "(function)"
t_OBJECT = "(object)"
t_NULL = "(null)"
t_COMMENTARY = r"\/{2,}"
t_START = "^\<\?(php)"
T_END = "(?>)$"
t_INT = "(int)"
t_INTEGER = "(integer)"
t_DOUBLE = "(double)"
t_FLOAT = "(float)"
t_CHAR = "(char)"
t_STRING = "(string)"
t_BOOLEAN = "(bool)"
t_IF = "(if)"
t_ELSE = "(else)"
t_FOR = "(for)"
t_FOREACH = "(foreach)"
t_WHILE = "(while)"
t_DO = "(do)"
t_SWITCH = "(switch)"
t_CASE = "(case)"
t_BREAK = "(break)"
t_DEFAULT = "(default)"
t_RETURN = "(return)"
t_ECHO = "(echo)"
t_TYPE = "(type)"
t_AS = "(as)"
t_CLASS = "(class)"
t_AND = "(and)"
t_OR = "(or)"
t_XOR = "(xor)"
t_SET = "(set)"
t_GET = "(get)"
t_SETTYPE = "(settype)"
t_COOKIES = "(cookies)"
t_SETCOOKIES = "(setcookies)"
t_POST = "(post)"
t_PUSH = "(push)"
t_REQUIRE = "(require)"
t_INCLUDE = "(include)"
t_DATETIME = "(datetime)"
t_DATE = "(date)"
t_TIME = "(time)"

#REGLAS COMPLEJAS
def t_NUMBER(token):
    r"[1-9][0-9]*"
    token.value = int(token.value)
    return token
def t_BIN(token):
    r"[0-1][0-1]*"
    return token
def t_CHAIN(token):
    r"\'[a-zA-Z]*\'"
    return token
def t_VARIABLE(token):
    r"^[$]([a-zA-z]*[0-9]*)*"
    return token


#DETECTOR DE ERROR
def t_error(t):
    print("Illegal character '%s' " % t.value[0])
    t.lexer.skip(1)


#CREADOR LEXER PARA ANALISIS LEXICO
lexer = lex.lex()


#CADENA INTRODUCIDA
texto = input("Introduzca una cadena: ")
lexer.input(texto)


#reglas de produccion

def p_expresion_binaria(t):
    '''expresion_numerica : expresion_numerica MAS expresion_numerica
                        | expresion_numerica MENOS expresion_numerica
                        | expresion_numerica POR expresion_numerica
                        | expresion_numerica DIVIDIDO expresion_numerica'''

def p_expresion_unaria(t):
    'expresion_numerica : MENOS expresion_numerica %prec UMENOS'


#VERIFICADOR DE REGLAS SIMPLES Y COMPLEJAS
while True:
    tok = lexer.token()
    if not tok:
        break 
    print(tok)
 