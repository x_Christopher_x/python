import ply.lex as lex
import ply.yacc as yacc

tokens=[
            'NUMERO',
            'MENOS',
            'MAS',
            'IGUAL',
            'CADENA',
            'TIPODATO',
            'VARIABLE',
        ]
#reglas
t_NUMERO=r"\d+"
t_MENOS=r"-"
t_MAS=r"\+"
t_IGUAL=r"="
#reglas complejas
def t_TIPODATO(t):
    r"(int|char|double|ushort|ushort|long|bool|float|decimal|object|string)"
    return t
def t_VARIABLE(t):
    r"[a-zA-Z_]+[0-9_]*"
    return t
    
#int a=3;

# Ignored characters
t_ignore = " \t"
    
def t_error(p):
      print("Error lexico %s"%p.value)
      print("Error lexico %s", p.lexer.lexpos)

#DECLARACION DE OBJETO PARA RECONOCER LOS TOKENS
lex.lex() #renderiza los tokens y reglas 

#REGLAS DE PRODUCCION
def p_asignacion(p):
    'positivos : TIPODATO VARIABLE IGUAL NUMERO'

#DECLARACION DE OBJETO PARA LA RECONOCER LA GRAMATICA
lex.lex() #renderiza las reglas de produccion

sintactico=yacc.yacc()

while True:
    try:
        s = input('entrada > ')
    except EOFError:
        break
    sintactico.parse(s)

# cadena=input("intro cadena")
# l.input(cadena)

# while True:
#     token=l.token()
#     print(token)
#     if not token:
#         break