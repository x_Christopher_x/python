#Cesar Aner Alcon Calleja S7315-6
from pgmpy.models import BayesianModel
from pgmpy.factors.discrete import TabularCPD
from pgmpy.inference import VariableElimination

modelo = BayesianModel([('Nota','Carta'),('Dificultad','Nota'),('Capacidad','Nota'),('Capacidad','Prueba')])

cpd_dif = TabularCPD(variable = 'Dificultad', variable_card=2, values=[[0.4],[0.6]], state_names={'Dificultad':['Dificil','Facil']})

cpd_cap = TabularCPD(variable = 'Capacidad', variable_card=2, values=[[0.3],[0.7]], state_names={'Capacidad':['Capaz','No Capaz']})

cdp_prueba = TabularCPD(variable = 'Prueba', variable_card=2, values=[[0.7,0.05],[0.3,0.95]], evidence=['Capacidad'], state_names={'Prueba':['Positivo','Negativo'],'Capacidad':['Capaz','No Capaz']}, evidence_card=[2])

cpd_nota = TabularCPD(variable = 'Nota', variable_card=3, values=[[0.5,0.9,0.3,0.05],[0.3,0.08,0.4,0.25],[0.2,0.02,0.3,0.7]], evidence=['Capacidad','Dificultad'], state_names={'Nota':['A','B','C'],'Capacidad':['Capaz','No Capaz'],'Dificultad':['Dificil','Facil']}, evidence_card=[2,2])

cpd_carta = TabularCPD(variable = 'Carta', variable_card=2,values = [[0.9,0.6,0.01],[0.1,0.4,0.99]],evidence = ['Nota'],state_names = {'Carta': ['Favorable','Desfavorable'],'Nota' : ['A','B','C']}, evidence_card = [3])

modelo.add_cpds(cpd_cap,cpd_dif,cdp_prueba,cpd_nota,cpd_carta)

#A) Que variables son independientes entre si
print(modelo.local_independencies(['Prueba','Dificultad','Capacidad','Nota','Carta']))
inferencia = VariableElimination(modelo)

#B) Cual es la probabilidad de obtener una carta de recomendación.
respuesta1=inferencia.query(['Carta'])
print(respuesta1)
#C) Cual es la probabilidad de que una asignatura sea difícil sabiendo que el estudiante saco
#   una B, y el estudiante saco en el test positivo?.
respuesta2 = inferencia.query(['Dificultad'],evidence={'Nota' : 'B','Prueba' : 'Positivo'})
print (respuesta2)
#D)  Cual es la probabilidad de obtener una carta de recomendación sabiendo que el test salio
#    positivo?.
respuesta3 = inferencia.query(['Carta'],evidence={'Prueba' : 'Positivo'})
print (respuesta3)