# Proyecto: Piedra, papel o tijeras
Es un juego clasico de azar para dos personas 

![Employee data](image/otrogrupo.gif)
## Descripción del Proyecto

piedra, papel o tijeras: un juego divertido de la vida cotidiana que pasamos a un codigo de python para simular el enfrentamiento contra la computadora por medio del azar
y asi poder elevar nuestro conocimiento en programacion

## Configuración y Ejecución del Proyecto

Para configurar y ejecutar el juego, sigue los siguientes pasos:

- Clona este repositorio en tu sistema local.
- Asegúrate de tener Python 3.8 instalado.
- Ejecuta el archivo import random.py para iniciar el juego: python import random.py

## Libro: PROGRAMA Y LIBERA TU POTENCIAL
El libro titulado 'Programa y Libera tu Potencial' ha sido nuestra guía fundamental, proporcionándonos un sólido fundamento en la comprensión de los elementos esenciales de los algoritmos y la programación en Python. Su enfoque en la liberación del potencial creativo a través de la codificación ha sido un pilar esencial en la concepción y desarrollo de este proyecto.

![Employee data](image/book.jpg)

Sobre los autores:
(http://programatupotencial.com)
## Agradecimientos

Queremos agradecer a nuestro docente, al decano de la facultad de ingeniería y a la Universidad Privada Domingo Savio - Sede (Santa Cruz) por su apoyo y orientación a lo largo de este proyecto.
## Cómo Contribuir
Como proyecto universitario, las contribuciones están limitadas a los miembros del equipo. Sin embargo, si encuentras algún error o tienes alguna sugerencia para mejorar el código o los análisis, no dudes en contactarte con el docente o el equipo de desarrollo.

Para cualquier pregunta o comentario, por favor contacta al correo electrónico (sc.jaime.zambrana.c@upds.net.bo).

## Equipo de desarrollo
[Universidad Privada Domingo Savio - Santa Cruz](https://www.facebook.com/UPDS.bo)

Decano de la Facultad:
- [Msc. Wilmer Campos Saavedra](https://www.facebook.com/wilmercampos1)

Docente:
- [PhD.  Jaime Zambrana Chacón](https://facebook.com/zambranachaconjaime)

Equipo de desarrollo:
- Rodrigo Caleb Guaristy Mendez
- Carlos Ulises Rodriguez Kalayky
- Denilson Baldellon
- Nicolas Castillo
