import pandas as pd
#import matplotlib as plt
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from scipy.stats import norm
from sklearn.preprocessing import StandardScaler
from scipy import stats
import os

#print (os.path.dirname(__file__)+"\\ejer_pandas.svg")
datos=pd.read_csv(os.path.dirname(__file__)+"\\tiendapcok.csv")
# res=sns.barplot(datos['precio'],datos['marca'])
t=np.arange(0,2,0.01)
seno=np.sin(2*np.pi*t)
coseno=np.cos(2*np.pi*t)
sns.lineplot(t,seno)
sns.lineplot(t,coseno)
plt.show()
