# import matplotlib.pyplot as plt
# import numpy as np
# x=np.linspace(-20,20,10)
# y=2*x+5
# plt.plot(x,y,'o')
# plt.show()


# Python code explaining 
# numpy.poly1d() 
  
# importing libraries 
import numpy as np 
from numpy import poly1d,polyfit 

x=[0,1,2,3,4,5,6,7,8,9]  
y=[8,6,10,20,36,58,86,120,160,206]  
p = poly1d(polyfit(x, y, deg=2), variable='x')  
print(p)  
