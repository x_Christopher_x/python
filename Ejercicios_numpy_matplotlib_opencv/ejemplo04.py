import numpy as np
import matplotlib.pyplot as plt
import cv2
import os

img = cv2.imread(os.path.dirname(__file__)+'\\santacruz.jpg')
gris =cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
bordes=cv2.Canny(gris,30,150)
plt.imshow(bordes)
plt.show()