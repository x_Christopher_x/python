import tkinter as tk
from PIL import Image,ImageTk
import cv2
import os

def c1(valor):
    # Crear una copia de la imagen original
    img_modificada = img.copy()
    print(img_modificada)    
    
    # Convertir la imagen modificada a formato PIL
    img_pil = Image.fromarray(cv2.cvtColor(img_modificada, cv2.COLOR_BGR2RGB))

    # Actualizar la imagen en el canvas
    canvas.img_tk = ImageTk.PhotoImage(img_pil)
    canvas.itemconfig(canvas, image=canvas.img_tk)


ventana=tk.Tk()
ventana.title("Tarea 4")
ventana.geometry("500x500")

img= cv2.imread(os.path.dirname(__file__)+'\\santacruz.jpg')
img=cv2.resize(img,(300,300))

deslizador=tk.Scale(ventana,from_=0, to=255,orient=tk.HORIZONTAL,command=c1)
deslizador.pack()


img=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
img= Image.fromarray(img)

canvas= tk.Canvas(ventana,width=img.width,height=img.height)
canvas.pack()
canvas.imagen_tk=ImageTk.PhotoImage(img)
canvas.create_image(0,0,anchor=tk.NW,image=canvas.imagen_tk)

ventana.mainloop()
