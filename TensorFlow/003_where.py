import tensorflow as tf
import numpy as np
x = tf.constant([[1,2],[3,4],[5,6]])
print("Tensor A",x)
y=tf.where(x[:]>6)
print(y)

#llenar valores de 0s. 
TZ=tf.zeros([10, 10], tf.int32)
print(TZ)

#Matriz identidad
XY=np.identity(5)
txy=tf.convert_to_tensor(XY)
print("______________")
print("numpy",XY)
print("______________")
print("Tensor",txy)
print("______________")
print("Tensor trasnspuesto",tf.transpose(txy))
#Llenar matriz de 5x5 con el valor 5 
mll=tf.fill([5,5],5)
print("______________")
print(mll)

#Llenar matriz utilizando split
x = tf.Variable(tf.random.uniform([5, 5], 0, 9,dtype=tf.int32))


